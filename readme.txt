for installing requirements:
---------------------------
pip install -r requirements.txt

for testing :
-------------
run test_stemmer_from_a_trained_model.py.ipynb
(you have to put your test sentence into the 'resource/test_lines.txt')


for trainig:
------------
run train_stemmer.py.ipynb
